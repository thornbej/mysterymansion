PC port of Mystery Mansion
==========================

mmm.exe - a practically exact port of the HP RTE version for dos
mmm - an x86 linux port with a few formatting fixes and compiler changes
dossrc directory contains fortran and C source for the dos version
linuxsrc directory contains fortran source for the linux version

The original port source and exe were supplied by Ken Cornetet, it
works under MsDos, DosEmu/FreeDos, Win95, WinXP etc. It was originally
compiled by an old version of MS QuickC, I had no luck recompiling using
about every free compiler I could find, but quite likely I wasn't doing
something right, using the wrong F2C utility, who knows. Just as well,
it's semi-historical just like it is.

I took the files and hacked around to get them to work with the
linux version of F2C, later Fortrans seemed to have forgotten some
of the things FORTRAN did like convert source text to double-bytes,
replaced those with constants given to me by oct_tool. On HP21xx minis
bits 8-15 of a word is the 1st character, bits 0-7 is the 2nd character.
Once I made those and other minor fixes the source compiled perfectly
using gcc 3.4.6. Funny thing when first testing it I thought I got
an early crash exit but all that happened was I blundered out an open
window and when that happens it prints text detailing the end and exits..
I had forgotten to arrange a pause to see the last thing printed.
So now I use a couple of scripts...

----------- mmm.sh -------------------------
#!/bin/bash
cd /mnt/hda11/f2cStuff/MMmod
konsole -e ./mmm1.sh
--------------------------------------------

----------- mmm1.sh ------------------------
#!/bin/bash
./mmm
read nothing
--------------------------------------------

...edit the cd command to point to where the files are, and if the
console terminal isn't named "konsole" (KDE's version) edit appropriately.
Konsole runs fine on Gnome desktops like I have, but to run Gnome's native
version replace with something like: gnome-terminal -e "./mmm1.sh"
Run mmm.sh to start, or make a desktop shortcut/launcher to it.

The source changes are detailed in the amods.txt file in the linuxsrc
directory, most were to fix spurious quotes in the output caused by some
lines being longer than the 72 characters allowed. In the vintage HP source
I found, columns 73-80 contained text like MMM00100 which overwrote some of
the comments, and also a few "," chars separating text fields. With the
continuation line the compiler sees "" and thinks it means print a quote.
Having the sequence strings made it very easy to find where the bugs were,
just had to search for "MMM and found 21 occurences (one was not an error).
It was fairly easy to key text from those lines into an editor search with
the port files loaded and move stuff around to restore the missing comma.
While at it fixed another spot which occurs on a screen early in the game
where the formatting/punctuation gets odd, one of those commas apparently
got moved to within the quotes.

The changes to the Linux version were restricted to text formatting, and
the Dos version is practically a copy of the original, so any logic bugs
from the HP1000 Mystery Mansion should be preserved in these versions.

This is free software and comes as-is without warrantee.

Terry Newton (wtn90125@yahoo.com)
